#!/usr/bin/env ruby

$t = Time.now

require 'optparse'
require 'json'
require 'yaml'
require 'logger'
require 'aws-sdk'
require 'net/ssh'
require 'net/scp'

def print_eoo
    printf "End of operation, duration: %.2fs\n", Time.now - $t       
end

def read_file(filename)
    IO.read(filename).to_s
end

class VM
    attr_accessor :dns_name
    private
    def exit_on_exception(e)
        $stderr.puts "#{e}"
    #    @ec2.instances[@options[:instance]].terminate
        print_eoo
        exit 1
    end
    def read_config(filename)
        begin
            return YAML.load(IO.read(filename))
        rescue Exception => e
            exit_on_exception "Couldn't open or read correctly #{filename}: #{e}"
        end
    end
    def load_instance
        begin
            instance_id = @options[:instance]
            puts "instance_id = #{instance_id}"
            instance = @ec2.instances[instance_id]
            raise "Couldn't find the ec2 instance #{instance_id}" unless instance.exists?
            @dns_name = instance.dns_name
        rescue Exception => e
            exit_on_exception "something unexpected happened while loading the instance: #{e}"
        end
    end
    def create_instance
        begin
            sg_name = 'dropwizard_scala'
            dropwizard_scala = @ec2.security_groups.find{ |x| x.name == sg_name}
            if dropwizard_scala.nil?
                dropwizard_scala = @ec2.security_groups.create(sg_name)
                dropwizard_scala.authorize_ingress(:tcp, 8080..8081) 
                dropwizard_scala.authorize_ingress(:tcp, 22) #ssh
            end
            instance = @ec2.instances.create(   :image_id => @options[:image_id],
                                                :kernel_id => @options[:kernel_id],
                                                :instance_type => @options[:instance_type],
                                                :security_groups => @ec2.security_groups[sg_name],
                                                :count => @options[:number_of_instance],
                                                :key_name => @options[:key_name])
            begin
                sleep 2
                puts "creating VM, current status: #{instance.status}"
            end while instance.status != :running
            begin
                sleep 2
                puts instance.status
            end
            @dns_name = instance.dns_name
            @options[:instance] = instance.id
        rescue Exception => e
            raise "Something unexpected happened: #{e}"
        end
    end
    def async_execute_cmd(ssh, command, expected_output=nil)
        ssh.open_channel do |channel|
            channel.request_pty do |ch, success|
                raise "could not obtain pty" unless success
            end
            channel.exec(command) do |ch, success|
                abort "could not execute command" unless success
                channel.on_data do |ch, data|
                   puts "#{dns_name}: #{data}"  if @options[:verbose]
                end
            end
        end
    end
    def sync_execute_cmd(ssh, command)
        ssh.exec!(command)
    end

    public
    def initialize(options)
        @options = options
        conf = read_config(@options[:cred_file])
        @ec2 = AWS::EC2.new(conf) or die "Couldn't authenticate to ec2, please check #{@options[:cred_file]}" 
        if @options[:instance].nil?
            @instance = create_instance
        else
            @instance = load_instance
        end
    end


    def install_and_configure
        retries = 3
        begin
            dropwizard = "helloworld"
            monitor = "poor_man_monitoring.rb"
            cron  = "monitoring-check"
            email = @options[:email]
            Net::SSH.start(@dns_name, @options[:user], :keys => @options[:key_file], :timeout => 600) do |ssh|
                # starting with a quick port scan to make sure nothing is binding those 2 ports
                port_scan = sync_execute_cmd(ssh, "nc -z localhost 8080-8081")
                raise "8080..8081 are alraedy used: #{port_scan}" if port_scan
                # uploading statics files
                ssh.scp.upload! dropwizard, dropwizard
                ssh.scp.upload! cron, cron
                ssh.scp.upload! monitor, monitor
                # close your eyes... it's ugly and not very safe as nothing is tested
                commands = ["sudo yum -y install java git jpackage-utils ruby-json",
                "rm -rf dropwizard-scala-sample ; git clone git://github.com/florianleibert/dropwizard-scala-sample.git",
                "sudo yum -y install java git jpackage-utils",
                "rm -rf dropwizard-scala-sample ; git clone git://github.com/florianleibert/dropwizard-scala-sample.git",
                "curl http://apache.claz.org/maven/maven-3/3.0.4/binaries/apache-maven-3.0.4-bin.tar.gz | tar xz",
                "sudo rm -rf /usr/lib/mvn/ ; sudo mv apache-maven-3.0.4 /usr/lib/mvn/",
                "cd dropwizard-scala-sample ; /usr/lib/mvn/bin/mvn package",
                "sudo rm -rf /var/lib/dropwizard-scala-sample ; sudo mv target /var/lib/dropwizard-scala-sample",
                "sudo mv sample.yml /var/lib/dropwizard-scala-sample",
                "sudo mv ~/#{dropwizard} /etc/init.d/",
                "sudo chkconfig --add #{dropwizard} ; cd", 
                "sed -i 's/EMAIL_TO/#{email}/' poor_man_monitoring.rb",
                "sudo mv ~/#{cron} /etc/cron.d"]
                async_execute_cmd(ssh, commands.join(";"))
            end
        rescue Errno::ECONNREFUSED
            if retries > 0 # crapy work around the fact that once the host is running its not necessarily allowing connections yet
                sleep 60
                retries -= 1
                retry
            end
            raise "Connection refused"
        rescue Exception => e
            exit_on_exception "Couldn't operate the vm correctly: #{e}"    
        end
    end
end

# default options
options = {}
options[:number_of_instance] = 1
options[:instance_type] = 't1.micro'
options[:image_id] = 'ami-1a249873'
options[:kernel_id] = 'aki-b6aa75df'
options[:verbose] = false
options[:user] = 'ec2-user'
# options provided in the commandline
optparse = OptionParser.new do |opts|
    opts.on("-k", "--key_name KEY_NAME", "AWS key_name (required)") do |kn|
        options[:key_name] = kn
    end
    opts.on("-p", "--pem_file PRIVATE_KEY", "private key file location (required)") do |pf|
        options[:key_file] = pf
    end
    opts.on("-c", "--cred_file CONF", "ec2 credentials file (required)") do |cred|
        options[:cred_file] = cred
    end
    opts.on("-e", "--email EMAIL", "Email to send alerts to (required)") do |mon|
        options[:email] = mon
    end
    opts.on("-i", "--instance INSTANCE_ID", "existing ec2 instance. If specified, -t, -n, --ami, --aki will be ignored (default none)") do |instance|
        options[:instance] = instance
    end
    opts.on("-t", "--instance_type TYPE", "EC2 instance type  (default #{options[:instance_type]})") do |instance_type|
        options[:instance_type] = instance_type
    end
    opts.on("-u", "--user UNIX_USER", "Unix user to perform ssh commands, this user needs sudoers NOPASSWD access (default #{options[:user]})") do |un|
        options[:username] = un
    end
    opts.on("-n", "--num_instance NUMBER", Integer, "number of instance to create (default #{options[:number_of_instance]})") do |number_of_instance|
        options[:number_of_instance] = number_of_instance
    end
    opts.on("-v", "--verbose", "add extra information to stderr (default #{options[:verbose]}))") do
        options[:verbose] = true
    end
    opts.on("--ami", "Specify AWS ami (default #{options[:image_id]})") do |ami|
        options[:image_id] = ami
    end
    opts.on("--aki", "Specify AWS aki (default #{options[:kernel_id]})") do |aki|
        options[:kernel_id] = aki
    end
    opts.on("-h", "--help", "display this screen") do
        puts opts 
        exit
    end 
 end

begin
    optparse.parse!
    mandatory = [:cred_file, :key_name, :key_file, :email]
    missing = mandatory.select{ |param| options[param].nil? }
    if not missing.empty?
        $stderr.puts "Missing options: #{missing.join(', ')}"
        $stderr.puts optparse
        exit
    end
rescue OptionParser::InvalidOption, OptionParser::MissingArgument
  $stderr.puts $!.to_s
  $stderr.puts optparse
  exit
end


if options[:verbose]
    AWS.config(:logger => Logger.new($stderr))
    AWS.config(:log_formatter => AWS::Core::LogFormatter.colored)
    $stderr.puts "Performing task with options: #{options.inspect}"
end

instance = VM.new options
instance.install_and_configure
puts "http://#{instance.dns_name}:8080/hello-world?name=Oskar"
print_eoo
