#!/usr/bin/env ruby
require 'open-uri'
require 'digest'
require 'json'
require 'net/smtp'

from = "nfelsen@gmail.com"
to = "EMAIL_TO"
output = []
test_url = "http://localhost:8080/hello-world?name=Oskar"
metrics_url = "http://localhost:8081/metrics"
expected_digest = "2248656c6c6f2c205365727675732c20c2a1686f6c61212c20e4bda0e5a5bd2c204368c3a06f2c20d097d0b4d180d0b0d0b2d181d182d0b2d183d0b9d182d0b5204f736b61722122"
digest = Digest.hexencode(open(test_url).read)

if digest != expected_digest
        output << "The ouput of #{test_url} didn't return the expected result"
end
metrics = JSON.load(open(metrics_url).read)

DATA.each { |data|
        x = data.split
        metric = x.shift
        metric_value = eval(metric).to_f
        comparator = x.shift
        limit = x.shift.to_f
        string = "#{metric_value} #{comparator} #{limit}"
        check = eval(string)
        output << "#{metric}: #{string}  => #{check}" unless check
}

exit 0 if output.length == 0

message = <<MESSAGE_END
From: #{from}
To: #{to}
Subject: Alert #{test_url} failing

The application HelloWord hosted at #{test_url} is not working properly\n#{output.join("\n")}
MESSAGE_END

Net::SMTP.start('localhost') do |smtp|
  smtp.send_message message, from, to
end 
    
__END__
metrics['jvm']['daemon_thread_count'] > 1
metrics["de.leibert.ExampleResource"]["sayHello"]["duration"]["p999"] > 0.05
metrics["org.eclipse.jetty.servlet.ServletContextHandler"]["percent-4xx-15m"]["value"] > 0.4
