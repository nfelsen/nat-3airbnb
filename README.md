Hello World
===========

Here is my “rushed” implementation of that exercise. I took the home-brew approach almost everywhere which, however, I don't think is the right thing to do.
The code is only compatible with redhat/centos/amazon distributions as it uses yum and rely on things like chkconfig to create a service.
The script was tested only on my laptop (a macbook pro, running ruby 1.9.3) and requires the following gems to be installed
    gem install json
    gem install yaml
    gem install aws-sdk
    gem install net-ssh
    gem install net-scp
    
In order to use the script, you need to clone the repo first.
this will give you the following 4 files:

* hello_world_manager.rb is the main script that takes care of starting the ec2 instance and making it prod ready. 
* config.example is an example of the json file expected by hello_word_manager
* helloworld is the init.d script turn hello world into a service
* poor_man_monitoring.rb is a small and easy to customize monitoring script that perform the requested checks. 

Using the script
-

You can see the usage using -h or no option:

    --$ ./hello_world_manager.rb
    Missing options: cred_file, key_name, key_file, email
    Usage: hello_world_manager [options]
        -k, --key_name KEY_NAME          AWS key_name (required)
        -p, --pem_file PRIVATE_KEY       private key file location (required)
        -c, --cred_file CONF             ec2 credentials file (required)
        -e, --email                      Email to send alerts to (required)
        -i, --instance INSTANCE_ID       existing ec2 instance. If specified, -t, -n, --ami, --aki will be ignored (default none)
        -t, --instance_type TYPE         EC2 instance type  (default t1.micro)
        -u, --user UNIX_USER             Unix user to perform ssh commands, this user needs sudoers NOPASSWD access (default ec2-user)
        -n, --num_instance NUMBER        number of instance to create (default 1)
        -v, --verbose                    add extra information to stderr (default false))
            --ami                        Specify AWS ami (default ami-1a249873)
            --aki                        Specify AWS aki (default aki-b6aa75df)
        -h, --help                       display this screen
    

As you can see the following 4 options are required:
* `cred_file` is the configuration file that will provide the keys to authenticate to aws.  At first I was planning on allowing all the options to be defined in that file but because of the time restriction, I quickly gave up on that. Right now that file should be contain at least the `access_key_id` and `secret_access_key` of your AWS account. I kept that out of the command line because those keys are too critical to be exposed into the shell history files (i.e. `.bash_history`).
* `key_name` and `key_file` are your pem file and the name of your key-pair which you should already have.
* the email option is for the monitoring.

here is an example of how to start the instance
    ./hello_world_manager.rb -k nat-playing-around -p ./nat-playing-around.pem  -c config   -e 'nfelsen@gmail.com'

you can also add the option -v to get a sense of what's happening. 



Under the hood
-

The creation of the ec2 instance is done using the official sdk. The script takes care of creating the security group and open the appropriate ports for the application to be accessible from the internet. 
The script then establishes an ssh connection to the vm. I had to add a retry system because when the vm gets into a running state, the ssh connection cannot yet be established.
At that point, I upload all the files I wrote and start an ugly sequence of command that will install yum packages, git clone the hello world application,  download and install maven, create the jar, then copy all the files to proper locations and finally start the service. If I were to do that again, I would use the appropriate tool for the job and create a cloudformation template instead. 
I chose to keep openjdk even so it’s not really common in production but since the application is simple and maven was ok with it, I felt that it wasn't an issue. 


Background process script
-
I created an init.d script to turn the application into a service
Unfortunately the init.d script has 2 bugs: 
* Somehow I'm not able to start the service in my ugly ssh block.
* I’m running daemon java … | logger (mostly because I’m out of time on that exercise) because of that the output daemon goes into syslog

The only option you have is that once the host is up to ssh into it and start the service: 

    --$ ssh -i  nat-playing-around.pem  ec2-user@ec2-184-73-103-190.compute-1.amazonaws.com
    [ec2-user@domU-12-31-39-0A-A5-A5 ~]$ sudo service helloworld start
    Starting helloworld controller:
    [ec2-user@domU-12-31-39-0A-A5-A5 ~]$ 

If I were to create a production web server, I would also stay away from combining, scp, yum, downloading maven from the internet etc… 
In my experience it's better to limit the number of source of truth. Therefore, I would create rpms for all the static files and use something like puppet to manage the configurations files and define which rpm get installed. That would also make the installation more reliable as you don't rely on github, apache.org etc... to be up.
In a production environement, I would also install something like monit and have it manage the service.

I would also have security concern regarding the fact that the web server has the port 22 open to everyone. In an idealistic environment, we would have gateways that have nothing critical and only those hosts would be allowed to ssh the web server. Those gateways could 2 factor authentication setup for stronger authentication.

Currently that host is a single point of failure, it would much safer to have a few instances of that vm running and have a load balancer in front.
Since there is no data stored, I would argue that this script is enough and we don't need to put any backup strategy in place

Monitoring
-

If you want to tune the monitoring, you can simply edit poor_man_monitoring.rb and make your changes at the bottom of the file. 
I like and don't like my technique. it's super easy to tune the monitoring but at the same time, I'm using eval with no verification at all which can be drangerous (i.e. you can get the script to run eval('exit()')) It would be safer to have those evals running in there own threads using $SAFE=3 but I didn't have enough to implement that.

If we were to monitor the application in production, I would definitely take a different approach:
* Currently there is no way to know that the application is accessible from the internet. 
* I would keep the metrics monitoring local but would defintely verify the content from external ips. I would also use something like pingdom to have more information about the sanity and performance of the application outside of the production network. 
* In addition to have an alerting mechanism based on therehold, I would also graph all those value to be able to get trends about the traffic on that host. 
* I didn't try to make it secure but It would better to have the monitoring script running under its own user
* Finally I don't like to keep the logs locally on a host, in production, I would use rsyslog to export the logs to a different host like a logger box and use a service like graylog2 or  splunk to index them.


MISC
-

While working on that project I found out about  netflix cloud manager tool and it looks really appropriate to run a big infrastructure on top of AWS. Here is the link http://techblog.netflix.com/2012/06/asgard-web-based-cloud-management-and.html I would definetely try to install it and see if I can take advantage of it. 



